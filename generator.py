import yaml
import templates


class RestrictedRelationError(Exception):
    pass


class RelationNotMutualError(Exception):
    pass


class Generator(object):

    def __init__(self):
        self.tables = []
        self.triggers = []
        self.relations = []
        self.many_to_many = set()

    def generate(self, file_name):
        with open(file_name, 'r') as source_file:
            self.schema = yaml.load(source_file)
            for entity, structure in self.schema.iteritems():
                table_name = entity.lower()
                self._generate_entity(table_name, structure)
                self._generate_relations(entity, structure)

    def _generate_entity(self, table_name, structure):
        placeholders = []

        for field_name, field_type in structure['fields'].iteritems():
            placeholders.append(
                self._generate_field(table_name, field_name, field_type)
            )

        self.triggers.append(self._generate_trigger(table_name))
        self.tables.append(
            self._generate_table(table_name, placeholders)
        )

    def _generate_table(self, table_name, placeholders):
        return templates.table.format(
            table_name=table_name,
            placeholders='\n    '.join(placeholders),
            timestamps=self._generate_timestamps(table_name)
        )

    def _generate_timestamps(self, table_name):
        return templates.timestamps.format(table_name=table_name)

    def _generate_field(self, table_name, field_name, field_type):
        return templates.field.format(
            table_name=table_name,
            field_name=field_name,
            field_type=field_type.upper())

    def _generate_trigger(self, table_name):
        return templates.trigger.format(table_name=table_name)

    def _generate_relations(self, table_name, structure):
        for related_table, relation in structure['relations'].iteritems():
            if table_name == related_table:
                raise RestrictedRelationError()

            if table_name not in self.schema[related_table]['relations']:
                raise RelationNotMutualError()

            if self.schema[table_name]['relations'][related_table] == 'many':
                if self.schema[related_table]['relations'][table_name] == 'many':
                    self._many_to_many(
                        table_name.lower(), related_table.lower()
                    )
                else:
                    self._one_to_many(
                        related_table.lower(), table_name.lower()
                    )

    def _many_to_many(self, left, right):
        tables = tuple(sorted((left, right)))

        if tables not in self.many_to_many:
            self.relations.append(templates.proxy_table.format(
                left=tables[0],
                right=tables[1]
            ))

            self.many_to_many.add(tables)

    def _one_to_many(self, left, right):
        self.relations.append(templates.alter_table.format(
            table_name=left,
            related_table=right
        ))

    def dump(self, file_name):
        with open(file_name, 'w') as output:
            output.write(''.join(self.tables))
            output.write(''.join(self.triggers))
            output.write(''.join(self.relations))

a = Generator()
a.generate("schema.yaml")
a.dump("output.sql")
