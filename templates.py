table = '''CREATE TABLE "{table_name}" ( 
    "{table_name}_id" SERIAL PRIMARY KEY,
    {placeholders}
    {timestamps}
);
'''

field = '''"{table_name}_{field_name}" {field_type}'''

alter_table = '''
ALTER TABLE "{table_name}" ADD COLUMN "{related_table}_id" INTEGER NOT NULL,
    ADD CONSTRAINT "fk_{table_name}_{related_table}_id" FOREIGN KEY ("{related_table}_id") REFERENCES "{related_table}" ("{related_table}_id");
'''
proxy_table = '''
CREATE TABLE "{left}__{right}" (
    "{left}_id" INTEGER NOT NULL FOREIGN KEY REFERENCES "{left}" ("{left}"_id),
    "{right}_id" INTEGER NOT NULL FOREIGN KEY REFERENCES "{right}" ("{right}"_id)
);
'''

timestamps = '''"{table_name}_created" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER), 
    "{table_name}_updated" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER)'''

trigger = '''
CREATE OR REPLACE FUNCTION update_{table_name}_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    NEW.{table_name}_updated = cast(extract(epoch from now()) as integer);
    RETURN NEW;
END;
$$ language \'plpgsql\';
CREATE TRIGGER "tr_{table_name}_updated" BEFORE UPDATE ON "{table_name}" FOR EACH ROW EXECUTE PROCEDURE update_{table_name}_timestamp();
'''
